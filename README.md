# SQLCheck

#### 介绍
功能为检查SQL文件夹中.sql文件的每条sql变更语句是否符合规范，避免sql语句影响其他系统

#### 软件架构
python3+PyQt5，使用工具：PyCharm + Qt Designer


#### 安装教程

1.  下载发行版后，需在exe同级目录建立SQL文件夹（可从此仓库中下载测试sql）
2.  然后将需要检查的sql文件放于此
3.  双击运行程序即可（目前仅在win10 64位环境测试）

#### 使用说明

1.  将需检查的sql文件放置于SQL文件中
2.  运行后点击右边按钮即可进行分析（默认检查），也可以在输入框输入需要检查的表名和对应的关键字段
3.  测试截图如下：
- 软件截图
![checkResults](https://gitee.com/yonghuizhong/sqlcheck/raw/master/img/checkResults.png)

- 测试案例  
![testCase1](https://gitee.com/yonghuizhong/sqlcheck/raw/master/img/testCase1.png)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
