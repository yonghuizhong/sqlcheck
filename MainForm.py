import sys
from PyQt5 import QtCore, QtGui
import os
import fnmatch
import re

from PyQt5.QtWidgets import QMainWindow, QApplication

from SQLCheckForm import Ui_MainWindow


# 检查逻辑由本人完成，待完善
# 可视化过程感谢CSDN博主"汪呀呀呀呀呀呀呀"的文章，界面较粗糙，待完善
# 开发时间：20210619
# 功能为检查SQL文件夹中.sql文件的每条sql语句是否包括特定关键词，避免sql语句影响其他系统

# 信号类，迎来发射标准输出作为信号
class EmittingStr(QtCore.QObject):
    # 定义一个发送str的信号
    textWritten = QtCore.pyqtSignal(str)

    def write(self, text):
        self.textWritten.emit(str(text))


# 主界面类
class MainForm(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainForm, self).__init__()
        self.setupUi(self)
        # 下面将输出重定向到textBrowser中
        sys.stdout = EmittingStr(textWritten=self.output_written)
        sys.stderr = EmittingStr(textWritten=self.output_written)

    # 接受信号str的信号槽
    def output_written(self, text):
        cursor = self.textBrowser.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText(text)
        self.textBrowser.setTextCursor(cursor)
        self.textBrowser.ensureCursorVisible()

    # 实现pushButton_click()函数，点击pushButton时调用
    def check_button_click(self):
        # 先进行清空
        self.textBrowser.setText("")
        # 获取窗口值
        table_str = self.TableNamelineEdit.text().strip()
        keys_str = self.KeyslineEdit.text().strip()
        # 调用检查函数，进行检查SQL文件夹中的sql文件
        self.check(table_str, keys_str)

    # 检查函数
    @staticmethod
    def check(table_str, keys_str):
        # 默认值的设定
        path = './SQL/'
        sql_spilt_symbol = ';|@@END@@'
        keys_spilt_symbol = ',|，'
        check_names = ['Field_Nod', 'Field_Job']
        check_table = 'tab_Text'
        sql_end_symbol = ';'
        # 如果不为空的话，从窗口获取变量值进行替换
        if table_str:
            check_table = table_str
        if keys_str:
            check_names = re.split(keys_spilt_symbol, keys_str)
        # 字符串全部转成大写进行比较
        check_names = [name.strip().upper() for name in check_names if name.strip() and isinstance(name, str)]
        check_table = check_table.upper()
        print('check fields: ' + str(check_names) + ' of ' + check_table)
        for f_name in os.listdir(path):
            if fnmatch.fnmatch(f_name, '*.sql'):
                print('Start checking file: ' + f_name)
                sql_num = 0
                wrong_num_of_keys = 0
                with open(path + f_name, 'rt') as f:
                    data = f.read().replace('\n', '')
                    sql_list = re.split(sql_spilt_symbol, data)
                    for sql in sql_list:
                        if sql:
                            sql_num += 1
                            check_sql = (sql + sql_end_symbol).upper()
                            # TODO sql的语法检查

                            # 如果sql语句中有该表名才进行检查关键字段
                            if check_table in check_sql:
                                if not all(name in check_sql for name in check_names):
                                    print('The ' + str(sql_num) + 'th is wrong: ' + check_sql)
                                    wrong_num_of_keys += 1
                print('End checking file, and the wrong nums of ' + f_name + ' is ' + str(wrong_num_of_keys) + '\n')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = MainForm()
    win.show()
    sys.exit(app.exec_())
